#ifndef __geo__
#define __geo__

#include "Sensor.h"
#include "Config.h"

#include <vector>
#include <array>
#include <map>
#include <unordered_map>

#include <TVector3.h>

class TVector2;

class geo {
public:
  static void init( const config& c );
  static void load_alignment( const config& c );

  static constexpr unsigned ndof = 8;
  enum params { Tx=0, Ty=1, Tz = 2, Rz=3, Ry=4, Rx=5, Sx=6, Sy=7 };

  static constexpr unsigned origin_plane = 0;
  
  enum class prealign { wo_prealign, with_prealign };
  enum class align    { wo_align,    with_align };
  enum class flip     { wo_flip,     with_flip };
  
  geo();
  geo( const std::string&name, SensorGeo::posfuncs funcs, std::string& type, const double& z, std::array<double, 4> matrix);
  TVector2 apply_fliprot( const TVector2& vpos ) const;
  TVector2 apply_fliprot_inv( const TVector2& ) const;
  void apply_alignment( TVector3& ) const;
  void apply_alignment_inv( TVector3& ) const;
  TVector3 pixel_to_global( const double& col, const double& row, prealign do_prealign = prealign::with_prealign, align do_align = align::with_align ) const;
  TVector2 pixel_to_local( const double& col, const double& row ) const;
  TVector2 global_to_local( const TVector3& global_pos ) const;
  TVector3 local_to_global( const TVector2& local_pos, align do_align = align::with_align ) const;
  TVector2 local_to_folded( const TVector2& local, const int& col, const int& row, flip do_flip = flip::with_flip ) const;
  TVector3 global_err() const;
  TVector3 local_err() const;
  void get_pixel( TVector2& local_pos, int& col, int& row ) const;
  SensorGeo::pos_func     position;
  SensorGeo::unc_func     uncertainty;
  SensorGeo::size_func    ncol;
  SensorGeo::size_func    nrow;
  SensorGeo::width_func   col_width;
  SensorGeo::width_func   row_width;
  SensorGeo::map_func     sensor_col;
  SensorGeo::map_func     sensor_row;
  SensorGeo::readout_func is_readout;
  SensorGeo::roi_func     is_roi;
  SensorGeo::flip_func    is_toFlip;
  std::array<double, 4> fliprot;
  static std::vector<unsigned> planes;
  static std::unordered_map<unsigned, geo> geometry;

  std::string plane_type;
  std::string sensor_name;
  TVector2 pre_align;
  TVector3 Trans;
  TVector3 Rot;
  TVector2 Scale;
  TVector3 Trans_err;
  TVector3 Rot_err;
  TVector2 Scale_err;
  TVector2 error_scale;

};

#endif
