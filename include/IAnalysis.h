#ifndef __Analysis__
#define __Analysis__

#include "Config.h"
#include <deque>
#include <memory>
#include <thread>
#include <mutex>
#include <TFile.h>
#include <TTree.h>

namespace AnaID {
  enum AnaID {
    HotPixelAnalysis = 0,
    Clustering       = 1,
    MaskMap          = 0,
    Correlator       = 1,
    AlignTracking    = 2,
    TrackAna         = 0
  };
};
    
class IAnalysis {
public:
  enum debug_option{ muted, info, debug, verbose, warning, error };
  static std::deque<std::unique_ptr<IAnalysis> > ana_queue;
  static int verbosity;
  static bool batch;
  static void play( const config& c, const std::string& ana_type, const std::string& input_file );

  template<class Analysis>
  static std::unique_ptr<IAnalysis> gen_analysis( const config& c, const std::string& name, const unsigned& id )
  {
    return std::unique_ptr<IAnalysis>( new Analysis( c, name, id ) );
  };

  static std::mutex mtx;

private:
  void processAll();

protected:
  const config& cfg;
  const std::string ana_name;
  const std::string ana_type;
  const unsigned order;
  long long nEntries_override;
  std::unique_ptr<TFile> file;
  TTree *tree;
  
  void loop();
  
  virtual void init()         = 0;
  virtual void initTree()     = 0;
  virtual void processEntry() = 0;
  virtual void end()          = 0;
  
public:
  
  IAnalysis( const config& c, const std::string& name, const std::string& type, const unsigned& id );
  virtual ~IAnalysis();
  
  inline const std::string& name() const { return ana_name; }
};

#endif
