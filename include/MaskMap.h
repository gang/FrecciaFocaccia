#ifndef __maskmap__
#define __maskmap__

#include "ClusterAnalysis.h"

class TH2F;

class MaskMap final : public ClusterAnalysis {
private:
  std::map<int, TH2F*> xymaps;
  
  void init()         override;
  void processEntry() override;
  void end()          override;
  
public:
  MaskMap( const config& c, const std::string& _filename, const unsigned& num );
  ~MaskMap();
};

#endif
