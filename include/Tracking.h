#ifndef __TRACKING__
#define __TRACKING__

#include <iostream>
#include <tuple>
#include <algorithm>
#include <memory>
#include "ClusterAnalysis.h"
#include "AlignMonitoring.h"
#include "alignfitter.h"
#include "track.h"


//____________________________________________________________________________________________________
class Tracking : public ClusterAnalysis, protected AlignMonitoring {
public:
  using hit_collection   = std::vector< std::shared_ptr<measurement> >;
  using measurement_bank = std::map<unsigned, hit_collection >;
  using track_collection = std::vector< std::shared_ptr<track> >;

protected:
  
  // Holder of hit maps
  TFile *maskmap_file;
  
  std::string maskfilename;
  std::string align_name;
  std::map<int, TH2F*> maskmaps;

  track_collection track_accum;
  
  bool is_masked( const cluster& c );
  void get_measurements( measurement_bank& );
  void find_tracks( measurement_bank& );
  void fit_track( std::shared_ptr<track>&, const int plane=-1 );
  void fit_tracks( const int nmax = -1, const int plane=-1 );
  void output();
  void summary1();
  void summary2();
  void fill_resmap();
  void fill_tree();
  
  virtual void init()  override;
  void processEntry()  override;
  virtual void end()   override;

  void loadInputs();
  
  Tracking( const config& c, const std::string& _ananame, const std::string& _filename, const unsigned& num );
  
public:
  Tracking( const config& c, const std::string& _filename, const unsigned& num );
  virtual ~Tracking();
  
};


#endif
