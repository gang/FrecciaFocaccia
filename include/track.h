#ifndef __track__
#define __track__

#include "clusterdata.h"
#include <TVector3.h>
#include <map>
#include <memory>

//____________________________________________________________________________________________________
class measurement {
public:
  measurement( const unsigned& _plane, TVector3 _hit );
  ~measurement() {}
  unsigned plane;
  TVector3 hit;
  TVector3 residual;
  std::shared_ptr<cluster>  clus;
  
  measurement* clone() const;
  void print() const;
};

//____________________________________________________________________________________________________
class track {
public:
  static constexpr unsigned ndof = 4;
  enum { active, branched, fit_bad };
  
  std::vector< std::shared_ptr<measurement> > measurements;
  double x0;
  double y0;
  double dxdz;
  double dydz;
  double chi2;
  unsigned status;
  track();
  track( const track& t );
  ~track();
  track* clone() const;
  void print() const;
  bool hasHitAt( const unsigned& plane ) const;
  unsigned numberOfHitsAt( const unsigned& plane ) const;
  void calc_residuals();
  TVector3 get_global( const unsigned& plane ) const;
};


#endif
