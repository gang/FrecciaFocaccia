#include "TrackData.h"

const hitdata& trackvars::data( const unsigned& ipl ) {
  d.plane    = plane    ->at( ipl );
  d.x        = x        ->at( ipl );
  d.y        = y        ->at( ipl );
  d.folded_x = folded_x ->at( ipl );
  d.folded_y = folded_y ->at( ipl );
  d.trkcol   = trkcol   ->at( ipl );
  d.trkrow   = trkrow   ->at( ipl );
  d.local_x  = local_x  ->at( ipl );
  d.local_y  = local_y  ->at( ipl );
  d.size     = size     ->at( ipl );
  d.sumToT   = sumToT   ->at( ipl );
  d.ave_col  = ave_col  ->at( ipl );
  d.ave_row  = ave_row  ->at( ipl );
  d.res_x    = res_x    ->at( ipl );
  d.res_y    = res_y    ->at( ipl );
  d.is_hit   = d.size>0? true : false;
  return d;
};

trackvars::trackvars()
  : chi2      ( 0 )
  , nhits     ( 0 )
  , nhits_tel ( 0 )
  , plane     ( new std::vector<int>    )
  , x         ( new std::vector<double> )
  , y         ( new std::vector<double> )
  , folded_x  ( new std::vector<double> )
  , folded_y  ( new std::vector<double> )
  , trkcol    ( new std::vector<int>    )
  , trkrow    ( new std::vector<int>    )
  , local_x   ( new std::vector<double> )
  , local_y   ( new std::vector<double> )
  , size      ( new std::vector<int>    )
  , sumToT    ( new std::vector<double> )
  , ave_col   ( new std::vector<double> )
  , ave_row   ( new std::vector<double> )
  , res_x     ( new std::vector<double> )
  , res_y     ( new std::vector<double> )
{}

trackvars::~trackvars() {
  delete plane;
  delete x;
  delete y;
  delete folded_x;
  delete folded_y;
  delete trkcol;
  delete trkrow;
  delete local_x;
  delete local_y;
  delete size;
  delete sumToT;
  delete ave_col;
  delete ave_row;
  delete res_x;
  delete res_y;
}


